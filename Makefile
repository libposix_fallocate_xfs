all: libs

clean:
	rm -f libposix_fallocate_xfs.so*

libs: posix_fallocate_xfs.c
	gcc -shared -Wl,-soname,posix_fallocate_xfs.so.1  -ldl -o libposix_fallocate_xfs.so.1.0  posix_fallocate_xfs.c -fPIC
	@rm -f libposix_fallocate_xfs.so.1 libposix_fallocate_xfs.so
	ln -s libposix_fallocate_xfs.so.1.0 libposix_fallocate_xfs.so.1
	ln -s libposix_fallocate_xfs.so.1 libposix_fallocate_xfs.so
