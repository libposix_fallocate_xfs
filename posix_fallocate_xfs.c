/*
    posix_fallocate_xfs preloadable library to make posix_fallocate
    use XFS' more efficient preallocation using unwritten extents

    Copyright © 2010 Bjoern JACKE <bjoern@j3e.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RTLD_NEXT
#  define _GNU_SOURCE
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dlfcn.h>
#include <xfs/xfs.h>


static int (*libc_posix_fallocate)(int, off_t, off_t)= NULL;

static void __attribute__ ((constructor)) posix_fallocate_xfs_init(void)
{
        libc_posix_fallocate= dlsym(RTLD_NEXT, "posix_fallocate");
        if (!libc_posix_fallocate || dlerror())
                _exit(1);
}

int posix_fallocate(int fd, off_t offset, off_t len)
{
	xfs_flock64_t fl;
	off_t new_len = offset + len;
	int ret;
	struct stat sbuf;

	if (new_len < 0)
		return EFBIG;

	fl.l_whence = SEEK_SET;
	fl.l_start = offset;
	fl.l_len = len;

	ret=xfsctl(NULL, fd, XFS_IOC_RESVSP, &fl);
	
	if (ret != 0)
		return errno;

	
	/* Make sure the space shows up when we enlarged the file: */
	fstat(fd,&sbuf);
	if (new_len > sbuf.st_size) {
		ftruncate(fd, new_len);
	}
	return 0;
}

int posix_fallocate64(int fd, off64_t offset, off64_t len)
{
	xfs_flock64_t fl;
	off64_t new_len = offset + len;
	int ret;
	struct stat64 sbuf;

	fl.l_whence = SEEK_SET;
	fl.l_start = offset;
	fl.l_len = len;

	ret=xfsctl(NULL, fd, XFS_IOC_RESVSP, &fl);

	if (ret != 0)
		return errno;

	/* Make sure the space shows up when we enlarged the file: */
	fstat64(fd,&sbuf);
	if (new_len > sbuf.st_size) {
		ftruncate64(fd, new_len);
	}
	return 0;
}

